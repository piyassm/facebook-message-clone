import React, { forwardRef } from "react";
import "./Message.css";
import { Card, CardContent, Typography } from "@material-ui/core";

const Message = forwardRef((props, ref) => {
  const { username, message, messagesOwn } = props;
  const isUser =
    (username === message.username && message.username !== "") ||
    (username === "" && messagesOwn.includes(message.text));

  return (
    <div className={`message ${isUser && "message__user"}`} ref={ref}>
      <Card
        className={`${isUser ? "message__userCard" : "message__guestCard"}`}
      >
        <CardContent>
          <Typography variant="h5" component="h2">
            {!isUser && `${message.username || "Unknown User"}:`} {message.text}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
});

export default Message;
