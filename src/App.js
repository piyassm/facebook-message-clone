import React, { useEffect, useState, useRef } from "react";
import "./App.css";
import {
  FormControl,
  Input,
  CircularProgress,
  IconButton,
} from "@material-ui/core";
import Message from "./Message";
import db from "./firebase";
import firebase from "firebase";
import FlipMove from "react-flip-move";
import SendIcon from "@material-ui/icons/Send";

const App = () => {
  const [messages, setMessages] = useState([]);
  const [messagesOwn, setMessagesOwn] = useState([]);
  const [loading, setLoading] = useState(true);
  const [input, setInput] = useState("");
  const [username, setUsername] = useState("");
  const bottomMessage = useRef(null);

  useEffect(() => {
    bottomMessage.current.scrollIntoView();
  }, [loading, messages]);

  const getMessages = () => {
    db.collection("messages")
      .orderBy("created", "asc")
      .onSnapshot((snapshot) => {
        setMessages(
          snapshot.docs.map((doc) => {
            return { id: doc.id, ...doc.data() };
          })
        );
        setLoading(false);
      });
  };

  useEffect(() => {
    getMessages();

    let name;
    // while (!name) {
    name = prompt("Please enter your name:");
    // }
    setUsername(name);
  }, []);

  const sendMessage = (e) => {
    e.preventDefault();
    db.collection("messages").add({
      username: username,
      text: input,
      created: firebase.firestore.FieldValue.serverTimestamp(),
    });
    setMessagesOwn([...messagesOwn, input]);
    setInput("");
  };

  return (
    <div className="app">
      <img
        src="https://facebookbrand.com/wp-content/uploads/2020/10/Logo_Messenger_NewBlurple-399x399-1.png?w=100&h=100"
        alt=""
      />
      <h1>Facebook Message Clone @mamaiank</h1>
      <h2>Welcome {`${username || "Unknown User"}`}</h2>
      <div className="app__boxMessage">
        <FlipMove>
          {!loading ? (
            messages.map((message, index) => {
              return (
                <Message
                  key={message.id}
                  username={username}
                  message={message}
                  messagesOwn={messagesOwn}
                />
              );
            })
          ) : (
            <div className="app__loading">
              <CircularProgress size={160} />
            </div>
          )}
        </FlipMove>
        <div ref={bottomMessage}></div>
      </div>
      <div className="app__boxForm">
        <form className="app__form" onSubmit={sendMessage}>
          <FormControl className="app__formControl">
            <Input
              placeholder="Enter a message..."
              value={input}
              onChange={(e) => setInput(e.target.value)}
              className="app__input"
            />
            <IconButton
              disabled={!!!input}
              variant="contained"
              color="primary"
              onClick={sendMessage}
              className="app__iconButton"
            >
              <SendIcon />
            </IconButton>
          </FormControl>
        </form>
      </div>
    </div>
  );
};

export default App;
