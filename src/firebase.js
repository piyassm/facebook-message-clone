import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCy07Nwzn9sdeljViNxP5fVZxznHlUHNio",
  authDomain: "facebook-message-clone-f070a.firebaseapp.com",
  databaseURL: "https://facebook-message-clone-f070a.firebaseio.com",
  projectId: "facebook-message-clone-f070a",
  storageBucket: "facebook-message-clone-f070a.appspot.com",
  messagingSenderId: "789149133372",
  appId: "1:789149133372:web:6f2dc77058154bd200f139",
  measurementId: "G-B6Q64B1M5W",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export default db;
